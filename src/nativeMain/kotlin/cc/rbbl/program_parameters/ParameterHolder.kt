package cc.rbbl.program_parameters

actual class ParameterHolder(parametersToLoad: Set<ParameterDefinition>) : BaseParameterHolder(parametersToLoad) {
    /**
     * loads the parameters from the system environment variables
     */
    actual fun loadParametersFromEnvironmentVariables() {
        TODO()
    }

}