package cc.rbbl.program_parameters

val firstPropertiesFileText = """
    Test_Key=first
    test_Key_two.Value=first
""".trimIndent()

val secondPropertiesFileText = """
    test_Key_two.Value=second
""".trimIndent()

val thirdPropertiesFileText = """
    Test_Key=third
""".trimIndent()