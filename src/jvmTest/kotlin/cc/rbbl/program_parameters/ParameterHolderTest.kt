package cc.rbbl.program_parameters

import uk.org.webcompere.systemstubs.environment.EnvironmentVariables
import java.io.File
import kotlin.test.Test
import kotlin.test.assertEquals


internal class ParameterHolderTest {
    @Test
    fun loadParametersFromPropertiesTest() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two.value", caseSensitive = false)
            )
        )
        testee.loadParametersFromPropertiesFile(
            this.javaClass.getResource("test.properties")!!.openStream()
        )
        assertEquals("first", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two.value")])
    }

    @Test
    fun loadParametersFromPropertiesFileTest() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two.value", caseSensitive = false)
            )
        )
        testee.loadParametersFromPropertiesFile(
            File(this.javaClass.getResource("test.properties")!!.file)
        )
        assertEquals("first", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two.value")])
    }

    @Test
    fun loadParametersFromPropertiesFileFolderTest() {
        //prepare
        File("tmp-tests").mkdir()
        val firstFile = File("tmp-tests/first.properties")
        firstFile.createNewFile()
        firstFile.writeText(firstPropertiesFileText)
        val secondFile = File("tmp-tests/second.properties")
        secondFile.createNewFile()
        secondFile.writeText(secondPropertiesFileText)
        val thirdFile = File("tmp-tests/third.properties")
        thirdFile.createNewFile()
        thirdFile.writeText(thirdPropertiesFileText)
        val fourthFile = File("tmp-tests/third.props") //should be ignored by FilenameFilter
        fourthFile.createNewFile()
        fourthFile.writeText(firstPropertiesFileText)

        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two.value", caseSensitive = false)
            )
        )

        //test
        testee.loadParametersFromPropertiesFile(File("tmp-tests"))
        assertEquals("third", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two.value")])

        //clean
        File("tmp-tests").deleteRecursively()
    }



    @Test
    fun dottedParametersTest() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("mail.username"),
                ParameterDefinition("mail.password")
            )
        )
        EnvironmentVariables().set("mail.username", "username").set("mail.password", "aPassword")
            .execute { testee.loadParametersFromEnvironmentVariables() }
        assertEquals("username", testee["mail.username"])
        assertEquals("aPassword", testee[ParameterDefinition("mail.password")])
    }

    @Test
    fun loadParametersFromEnvTest() {
        val testee = ParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two", caseSensitive = false)
            )
        )
        EnvironmentVariables().set("Test_Key", "first").set("test_key_TWO", "second")
            .execute { testee.loadParametersFromEnvironmentVariables() }
        assertEquals("first", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two")])
    }
}