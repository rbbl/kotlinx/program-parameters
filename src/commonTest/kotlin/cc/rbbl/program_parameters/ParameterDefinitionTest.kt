package cc.rbbl.program_parameters

import kotlin.test.*

internal class ParameterDefinitionTest {

    @Test
    fun initializationTestEmptyKey() {
        assertFailsWith(IllegalArgumentException::class) { ParameterDefinition("") }
    }

    inner class CaseSensitive {
        lateinit var testee: ParameterDefinition

        @BeforeTest
        fun prepareTests() {
            testee = ParameterDefinition("Test_Key")
        }

        @Test
        fun matchesTestPositive() {
            assertTrue(testee.matches("Test_Key"))
        }

        @Test
        fun matchesTestNegative() {
            assertFalse(testee.matches("Test_key"))
        }

        @Test
        fun matchesTestNegativeTwo() {
            assertFalse(testee.matches("Test_ke"))
        }
    }

    inner class CaseInsensitive {
        lateinit var testee: ParameterDefinition

        @BeforeTest
        fun prepareTests() {
            testee = ParameterDefinition("Test_Key", caseSensitive = false)
        }

        @Test
        fun matchTestPositiveExact() {
            assertTrue(testee.matches("Test_Key"))
        }

        @Test
        fun matchesTestPositiveRough() {
            assertTrue(testee.matches("test_keY"))
        }

        @Test
        fun matchTestNegative() {
            assertFalse(testee.matches("Test_ke"))
        }
    }
}