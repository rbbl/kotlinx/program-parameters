package cc.rbbl.program_parameters

import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertFails
import kotlin.test.assertFailsWith


internal class BaseParameterHolderTest {
    @Test
    fun initializationTestPositive() {
        BaseParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("test_key"),
                ParameterDefinition("Test_Key_two")
            )
        )
    }

    @Test
    fun initializationTestNegative() {
        val exception = assertFailsWith(IllegalArgumentException::class) {
            BaseParameterHolder(
                setOf(
                    ParameterDefinition("Test_Key"),
                    ParameterDefinition("test_key"),
                    ParameterDefinition("Test_Key", caseSensitive = false),
                    ParameterDefinition("Test_Key_two"),
                    ParameterDefinition("Test_Key_two", caseSensitive = false),
                    ParameterDefinition("Test_Key_two", caseSensitive = false, required = false)
                )
            )
        }
        assertEquals("Duplicate Keys found: Test_Key, Test_Key_two", exception.message)
    }

    @Test
    fun loadParametersFromArgsTest() {
        val testee = BaseParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two", caseSensitive = false)
            )
        )
        val testParams = arrayOf("Test_Key=first", "test_key_TWO=second")
        testee.loadParameters(testParams)
        assertEquals("first", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two")])
    }

    @Test
    fun loadParametersFromPairSetTest() {
        val testee = BaseParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two", caseSensitive = false)
            )
        )
        testee.loadParameters(setOf(Pair("Test_Key", "first"), Pair("test_key_TWO", "second")))
        assertEquals("first", testee["Test_Key"])
        assertEquals("second", testee[ParameterDefinition("Test_Key_two")])
    }

    @Test
    fun checkParameterCompletenessTestNegative() {
        val testee = BaseParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two"),
                ParameterDefinition("Test_Key_three")
            )
        )
        testee.loadParameters(setOf(Pair("Test_Key", "first")))
        val exception = assertFailsWith(IllegalArgumentException::class) { testee.checkParameterCompleteness() }
        assertEquals("Missing Parameters: Test_Key_two, Test_Key_three", exception.message)
    }

    @Test
    fun checkParameterCompletenessTestNegativeEmpty() {
        val testee = BaseParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two"),
                ParameterDefinition("Test_Key_three")
            )
        )
        testee.loadParameters(setOf(Pair("Test_Key", "first"), Pair("Test_Key_two", "")))
        val exception = assertFailsWith(IllegalArgumentException::class) { testee.checkParameterCompleteness() }
        assertEquals("Missing Parameters: Test_Key_two, Test_Key_three", exception.message)
    }

    @Test
    fun checkParameterCompletenessTestPositive() {
        val testee = BaseParameterHolder(
            setOf(
                ParameterDefinition("Test_Key"),
                ParameterDefinition("Test_Key_two")
            )
        )
        testee.loadParameters(setOf(Pair("Test_Key", "first"), Pair("Test_Key_two", "second")))
        testee.checkParameterCompleteness()
    }
}