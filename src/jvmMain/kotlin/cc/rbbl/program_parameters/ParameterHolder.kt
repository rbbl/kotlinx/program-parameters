package cc.rbbl.program_parameters

import java.io.File
import java.io.InputStream
import java.util.*

/**
 * Object to load parameters from different sources based on the passed [ParameterDefinition]s
 *
 * Any loading operations performed may override values loaded from previous loading operations
 *
 * @param parametersToLoad parameter definitions to be handled by this [BaseParameterHolder]
 * @constructor instantiates a [BaseParameterHolder]
 */
actual class ParameterHolder(parametersToLoad: Set<ParameterDefinition>): BaseParameterHolder(parametersToLoad) {

    /**
     * loads parameters from a properties file
     *
     * @param inputStream of a valid .properties file
     */
    fun loadParametersFromPropertiesFile(inputStream: InputStream) {
        val properties = Properties()
        properties.load(inputStream)
        loadParameters(properties.entries.map { Pair(it.key.toString(), it.value.toString()) }
            .toSet())
    }

    /**
     * loads parameters from the given property file.
     * if a folder gets passed then all properties files within the folder get loaded in alphabetical order.
     *
     * @param fileOrFolder a File representing an individual properties file or a folder containing some
     */
    fun loadParametersFromPropertiesFile(fileOrFolder: File) {
        val properties = Properties()
        if (fileOrFolder.isFile) {
            properties.load(fileOrFolder.inputStream())
        } else {
            val filesInFolder = fileOrFolder.listFiles(PropertiesFileNameFilter())
            if (filesInFolder != null) {
                for (file in filesInFolder) {
                    properties.load(file.inputStream())
                }
            }
        }
        loadParameters(properties.entries.map { Pair(it.key.toString(), it.value.toString()) }
            .toSet())
    }

    /**
     * loads the parameters from the system environment variables
     */
    actual fun loadParametersFromEnvironmentVariables() {
        loadParameters(System.getenv().map { Pair(it.key, it.value) }.toSet())
    }
}