package cc.rbbl.program_parameters

import java.io.File
import java.io.FilenameFilter

class PropertiesFileNameFilter : FilenameFilter {
    override fun accept(file: File?, name: String?): Boolean {
        return name?.endsWith(".properties") ?: false
    }
}